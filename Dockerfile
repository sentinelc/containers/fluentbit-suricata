FROM docker.io/fluent/fluent-bit:2.2.0-debug

RUN rm /fluent-bit/etc/*
COPY fluent-bit.conf /fluent-bit/etc/fluent-bit.conf
COPY suricata/ /fluent-bit/etc/suricata/
COPY --chmod=755 start.sh /fluent-bit

WORKDIR /fluent-bit
CMD ["/fluent-bit/start.sh"]
