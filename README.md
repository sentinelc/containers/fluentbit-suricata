This is a side-kick container for the main suricata container.

Accepts the following ENV vars. See the `fluent-bit.conf` config file and
the fluent bit elastic search output documentation for meaning:
https://docs.fluentbit.io/manual/pipeline/outputs/elasticsearch

- HOSTNAME
- ES_HOST
- ES_PORT
- ES_CLOUD_ID
- ES_CLOUD_AUTH
- ES_HTTP_USER
- ES_HTTP_PASSWD
- ES_INDEX_PREFIX
- ES_TLS
- ES_TLS_VERIFY
- ES_CA_CERT
